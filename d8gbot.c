#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <openssl/sha.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "./configuration.h"

#define BUFFSIZE 8192

char *mystrdup(char *str);
char *next_message(int socket);
char *get_nick(char *msg);
int authorized(char *password);
void connect_to_irc(int *socket, struct sockaddr_in *socket_conf);
void connect_to_server(int socket, struct sockaddr_in *socket_conf);
void disconnect(int socket);
int init_socket(void);
void login(int socket);
void main_loop(int socket);
void send_raw_msg(int socket, char *message);
void set_socket(struct sockaddr_in *socket_conf);

char *mystrdup(char *str)
{
	char *copy = malloc((strlen(str) + 1) * sizeof(char));
	if (copy == NULL) {
		perror("malloc");
		abort();
	}
	strcpy(copy, str);
	return copy;
}

int authorized(char *password)
{
	int i, authorized = 1;
	char buff[BUFFSIZE], *p;
	unsigned char *hash = SHA1(password, strlen(password), NULL);
	int hash_len = strlen(hash);
	int PASS_len = strlen(PASSWORD);
	p = buff;
	for (i = 0; i < hash_len; i++, p += 2)
		snprintf (p, 3, "%02x", hash[i]);
#ifdef DEBUG
	fprintf(stderr, "\tPassword: '%s'\n", password);
	fprintf(stderr, "\tHash:     '%s'\n", buff);
	fprintf(stderr, "\tExpected: '%s'\n", PASSWORD);
#endif
	if (PASS_len != strlen(buff)) {
		authorized = 0;
	} else {
		for (i = 0; i < PASS_len; i++)
			if (PASSWORD[i] != buff[i])
				authorized = 0;
	}
	return authorized;
}

int init_socket(void)
{
	int s = socket(PF_INET, SOCK_STREAM, 6);
	if (s == -1) {
		perror("socket");
		fputs("Opening inet (v4) stream socket failed.\n", stderr);
		abort();
	}
	return s;
}

void disconnect(int socket)
{
	if (close(socket) == -1) {
		perror("close");
		abort();
	}
}

void set_socket(struct sockaddr_in *socket_conf)
{
	/* Translate IRCNET domain name to IP adress */
	struct hostent *host;
	if ((host = gethostbyname(IRCNET)) == NULL) {
		perror("gethostbyname");
		fprintf(stderr, "Cannot get IP address for domain: '%s'\n",
				IRCNET);
		abort();
	}
	/* Socket settings */
	socket_conf->sin_family = PF_INET;
	socket_conf->sin_port = htons((uint16_t) IRCPORT);
	socket_conf->sin_addr = *((struct in_addr *)host->h_addr_list[0]);
	memset(&(socket_conf->sin_zero), '\0', 2);
}

void connect_to_server(int socket, struct sockaddr_in *socket_conf)
{
	if (connect(socket, (struct sockaddr *)socket_conf,
				(socklen_t) sizeof(struct sockaddr)) == -1) {
		perror("connect");
		fputs("Connection attempt failed.\n", stderr);
		abort();
	}
}

void send_raw_msg(int socket, char *message)
{
#ifdef DEBUG
	fprintf(stderr, "Sending message: %s", message);
#endif
	if (write(socket, message, strlen(message))
			!= strlen(message)) {
		perror("write");
#ifdef DEBUG
		fputs("Sending message failed!", stderr);
#endif
		abort();
	}
}

void login(int socket)
{
	send_raw_msg(socket, "USER " BOTNICK " " BOTNICK ": noname noname\n");
	send_raw_msg(socket, "NICK " BOTNICK "\n");
	if (sleep(1U) > 0U) {
		perror("usleep");
		abort();
	}
	send_raw_msg(socket, "JOIN #" IRCROOM "\n");
}

void connect_to_irc(int *socket, struct sockaddr_in *socket_conf)
{
	*socket = init_socket();
	set_socket(socket_conf);
	connect_to_server(*socket, socket_conf);
	login(*socket);
}

char *next_message(int socket)
{
	char *complete_msg;
	int offset = 0, end = 0, i, msg_lenght;
	char buffer[BUFFSIZE];
	memset(buffer, '\0', BUFFSIZE);
	do {
		msg_lenght = read(socket, (void *) (buffer + offset),
					(BUFFSIZE - offset));
		if (msg_lenght == -1) {
			fprintf(stderr, "Socket read error.\n");
			disconnect(socket);
			abort();
		}
		offset += msg_lenght;

		for (i = 0; i < BUFFSIZE -1; i ++) {
			if (buffer[i] == '\r' && buffer[i+1] == '\n') {
				end = 1;
				break;
			}
		}
	} while (offset != BUFFSIZE && end == 0);

	if (end == 0) {
		fputs("Incoming message is badly formated.\n", stderr);
		disconnect(socket);
		abort();
	}

	buffer[i] = '\0';
	complete_msg = mystrdup(buffer);

#ifdef DEBUG
	fprintf(stderr, "Incoming msg: '%s'\n", complete_msg);
#endif
	return complete_msg;
}

char *get_nick(char *msg)
{
	unsigned i = 0;
	char *nick = malloc(sizeof(char) * strlen(msg));
	if (nick == NULL) {
		perror("malloc");
		abort();
	}
	/* skipping leading ':' */
	while (*++msg != '!') {
		if (*msg == '\0') {
			fputs("Incoming message invalid. Exiting.\n", stderr);
			abort();
		}
		*(nick + i) = *msg;
		i++;
	}
	*(nick + i) = '\0';
#ifdef DEBUG
	fprintf(stderr, "\tFrom nick: '%s'\n", nick);
#endif
	return nick;
}

void main_loop(int socket)
{
	char *msg, *nick;
	char buff[BUFFSIZE];
LOOP:
	msg = next_message(socket);  /* blocking */
	if (msg == NULL) {
		disconnect(socket);
		abort();
	}
	if (strstr(msg, "PRIVMSG " BOTNICK) != NULL) {
		if (strstr(msg, "quit") != NULL) {
			if (authorized(strstr(msg, "quit") + 5) == 1) {
				free(msg);
				msg = NULL;
				goto END;
			}
		} else if (strstr(msg, "ping") != NULL) {
			nick = get_nick(msg);
			if (snprintf(buff, BUFFSIZE, "PRIVMSG %s :pong\n", nick) < 0) {
				perror("snprintf");
				abort();
			}
			send_raw_msg(socket, buff);
			free(nick);
		} else if (strstr(msg, ":op ") != NULL) {
			if (authorized(strstr(msg, "op") + 3)) {
				nick = get_nick(msg);
				if (snprintf(buff, BUFFSIZE, "MODE #%s +o %s\n", IRCROOM, nick) < 0) {
					perror("");
					abort();
				}
				send_raw_msg(socket, buff);
				free(nick);
			}
		}
	} else {
		/* server ping request */
		if (strstr(msg, "PING :") != NULL) {
			if (snprintf(buff, BUFFSIZE, "PONG :%s\n", (strstr(msg, " :")+2)) < 0) {
				perror("snprintf");
				abort();
			}
			send_raw_msg(socket, buff);
		}
	}
	free(msg);
	msg = NULL;
	goto LOOP;
END:
	send_raw_msg(socket, "QUIT :\n");
}

int main(void)
{
	int socket;
	struct sockaddr_in socket_conf;
	connect_to_irc(&socket, &socket_conf);
	main_loop(socket);
	disconnect(socket);
	return 0;
}
